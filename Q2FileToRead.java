package com.dkit.gd2.antanaszalisauskas;

public class Q2FileToRead
{
    public static void main(String[] args) {
        int x = 10;
        int y = 12;
        double z = 8.89;
        for(int i = 0; i < 10; i++)
        {
            x += 1;
        }
        while(y < 100)
        {
            y += 15;
        }
        if(y > x)
        {
            System.out.println("Sample text");
        }
        else if(x < y)
        {
            System.out.println("More sample text");
        }
        else
        {
            System.out.println("Even more sample text");
        }
    }
}

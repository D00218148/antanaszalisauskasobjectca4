package com.dkit.gd2.antanaszalisauskas;

import java.util.*;

public class Q1Main
{
    private static Set<Employee> employees = new HashSet();
    private static boolean quit = false;
    private static Scanner sc = new Scanner(System.in);

    private enum MenuOptions
    {
        SHOWOPTIONS,
        STAFF,
        ADDSTAFF,
        CHANGECLEARANCE,
        QUIT,
    }

    private enum editClearance
    {
        ADD,
        REMOVE
    }

    public static void main(String[] args)
    {
        MenuOptions[] options = MenuOptions.values();

        //Adding sample employees
        Employee emp = new Employee(IDGenerator(), "Gerard Cole", "Photo of Gerard", "Programming Department");
        employees.add(emp);
        Employee emp2 = new Employee(IDGenerator(), "Jeremy Long", "Photo of Jeremy", "Art Department");
        employees.add(emp2);
        Employee emp3 = new Employee(IDGenerator(), "Kevin Dawn", "Photo of Kevin", "Marketing Department");
        employees.add(emp3);

        //adding security clearance to sample staff
        emp.addSecurityClearance(-1);
        emp.addSecurityClearance(0);
        emp2.addSecurityClearance(2);
        emp2.addSecurityClearance(1);
        emp3.addSecurityClearance(0);
        emp3.addSecurityClearance(1);
        emp3.addSecurityClearance(2);


        System.out.println("Sample Company's Security System Enter an Option From Below:");
        showOptions();

        try
        {
            while(!quit)
            {
                int choice = choiceInputInt();
                switch(options[choice - 1])
                {
                    case SHOWOPTIONS:
                        showOptions();
                        break;
                    case STAFF:
                        displayStaff();
                        showOptions();
                        break;
                    case ADDSTAFF:
                        addStaff();
                        break;
                    case CHANGECLEARANCE:
                        addClearance();
                        showOptions();
                        break;
                    case QUIT:
                        System.out.println("Quitting system");
                        quit = true;
                }
            }
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IndexOutOfBoundsException e)
        {
            System.out.println("Value entered is invalid or outside option range");
        }

    }

    private static void addStaff()
    {
        System.out.println("Enter staff name");
        String name = choiceInputString();
        System.out.println("Enter staff photo");
        String photo = choiceInputString();
        System.out.println("Enter staff department");
        String department = choiceInputString();

        Employee temp = new Employee(IDGenerator(), name, photo, department);
        boolean exists = checkStaff(temp);
        if(exists)
        {
            System.out.println("Staff ID already exists");
        }
        else
        {
            employees.add(temp);
            System.out.println("Staff added");
        }
        showOptions();
    }

    private static boolean checkStaff(Employee emp)
    {
        boolean exists = false;
        for(Employee employee: employees)
        {
            exists = employee.equals(emp);
            if(exists);
            {
                break;
            }
        }
        return exists;
    }

    private static String IDGenerator()
    {
        return "STF" + (int)(Math.random()*5000);
    }

    private static int choiceInputInt()
    {
        int choice = 0;
        try
        {

            System.out.println("Enter your choice");
            choice = sc.nextInt();
            return choice;
        }
        catch(InputMismatchException e)
        {
            System.out.println("Invalid input. Please enter a valid option.");
            sc.nextInt();
            choiceInputInt();
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program");
        }
        return choice;
    }

    private static String choiceInputString()
    {
        String choice = null;
        try
        {
            System.out.println("Enter your choice");
            choice = sc.nextLine();
        }
        catch(InputMismatchException e)
        {
            System.out.println("Invalid input. Please enter a valid option.");
            sc.nextLine();
            choiceInputString();
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program");
        }
        return choice;
    }

    private static void showOptions()
    {
        System.out.println("Enter 1 to display available options");
        System.out.println("Enter 2 to display staff details");
        System.out.println("Enter 3 to add a new staff");
        System.out.println("Enter 4 to add staff security clearance");
        System.out.println("Enter 5 to quit");
    }

    private static void displayStaff()
    {
        sc.nextLine();
        System.out.println("Currently signed in employees");
        for(Employee emp: employees)
        {
            System.out.println(emp.getID());
        }
        System.out.println("Enter employee ID for more details");
        String id = sc.nextLine();
        for(Employee emp: employees)
        {
            if(emp.getID().equals(id.toUpperCase()))
            {
                System.out.println(emp.toString());
                emp.listSecurityClearance();
            }
        }
    }

    private static void addClearance()
    {
        sc.nextLine();
        displayStaff();
        System.out.println("Enter staff ID to add clearance for");
        String choice = choiceInputString();

        for(Employee emp: employees)
        {
            if(emp.getID().equals(choice.toUpperCase()))
            {
                System.out.println("Enter security clearance to add (-1, 0, 1, 2)");
                int clearance = choiceInputInt();
                boolean added = emp.addSecurityClearance(clearance);
                if(added)
                {
                    System.out.println("Clearance added");
                }
                else
                {
                    System.out.println("Staff already has this clearance or clearance provided doesn't exist");
                }
                break;
            }
        }
    }
}


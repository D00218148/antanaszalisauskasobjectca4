package com.dkit.gd2.antanaszalisauskas;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class Employee
{
    private final String ID;
    private final String name;
    private final String photo;
    private final String department;
    private final List<Integer> securityClearance;

    public Employee(String ID, String name, String photo, String department) {
        this.ID = ID;
        this.name = name;
        this.photo = photo;
        this.department = department;
        this.securityClearance = new ArrayList();
    }

    public String getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDepartment() {
        return department;
    }

    public List<Integer> getSecurityClearance() {
        return new ArrayList<Integer>(securityClearance);
    }

    public void listSecurityClearance() {
        if(this.securityClearance.size() > 0)
        {
            System.out.println(this.name + " security clearance: ");
            for(int i = 0; i < this.securityClearance.size(); i++)
            {
                if(i != this.securityClearance.size() - 1)
                {
                    System.out.print(this.securityClearance.get(i) + ", ");
                }
                else
                {
                    System.out.print(this.securityClearance.get(i));
                }
            }
            System.out.println();
        }
        else
        {
            System.out.println(this.name + " doesn't have any security clearance");
        }
    }

    public final boolean addSecurityClearance(int clearance)
    {
        if(this.securityClearance.indexOf(clearance) > -1)
        {
            return false;
        }
        else if(this.securityClearance.indexOf(clearance) >= 0)
        {
            return false;
        }
        else
        {
            this.securityClearance.add(clearance);
            return true;
        }
    }
/* Main requires more work to add ability to remove security
    public boolean removeSecurityClearance(int clearance)
    {
        if(this.securityClearance.indexOf(clearance) > -1)
        {
            this.securityClearance.remove(clearance);
            return true;
        }
        else
        {
            return false;
        }
    }

 */

    @Override
    public String toString() {
        return "ID: '" + ID + '\'' +
                ", name: '" + name + '\'' +
                ", photo: '" + photo + '\'' +
                ", department: '" + department;
    }

    public boolean equals(Employee emp) {
        if(this.ID.equals(emp.getID()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.ID.hashCode() + 57;
    }
}

package com.dkit.gd2.antanaszalisauskas;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Q2
{
    public static void main(String[] args)
    {
        int intCount = 0;
        int doubleCount = 0;
        int forCount = 0;
        int whileCount = 0;
        int ifCount = 0;
        int lineNumber = 1;
        int identifierCount = 0;
        List<Integer> ints = new ArrayList();
        List<Integer> doubles = new ArrayList();
        List<Integer> forLoops = new ArrayList();
        List<Integer> whileStatements = new ArrayList();
        List<Integer> ifStatements = new ArrayList<>();
        List<String> identifiers = new ArrayList();

        File file = new File("Q2FileToRead.java");
        try(Scanner sc = new Scanner(new BufferedReader(new FileReader(file))))
        {
            while(sc.hasNext())
            {
                sc.useDelimiter("[^A-Za-z0-9_]+");
                String input = sc.next();
                //System.out.println(input); prints out the input
                if(input.contains("int") && !input.contains("for") && !input.contains("print"))
                {
                    if(!identifiers.contains(input))
                    {
                        identifiers.add(input);
                        identifierCount++;
                    }
                    ints.add(lineNumber);
                    intCount++;
                }
                else if(input.contains("double"))
                {
                    if(!identifiers.contains(input))
                    {
                        identifiers.add(input);
                        identifierCount++;
                    }
                    doubles.add(lineNumber);
                    doubleCount++;
                }
                else if(input.contains("for"))
                {
                    if(!identifiers.contains(input))
                    {
                        identifiers.add(input);
                        identifierCount++;
                    }
                    forLoops.add(lineNumber);
                    forCount++;
                }
                else if(input.contains("while"))
                {
                    if(!identifiers.contains(input))
                    {
                        identifiers.add(input);
                        identifierCount++;
                    }
                    whileStatements.add(lineNumber);
                    whileCount++;
                }
                else if(input.contains("if"))
                {
                    if(!identifiers.contains(input))
                    {
                        identifiers.add(input);
                        identifierCount++;
                    }
                    ifStatements.add(lineNumber);
                    ifCount++;
                }
                lineNumber++;

            }
            Collections.sort(identifiers);

            System.out.println("Identifiers [" + identifierCount + "] line numbers:");
            for(int i = 0; i < identifiers.size(); i++)
            {
                System.out.print(identifiers.get(i) + "(");
                if(identifiers.get(i).equals("int"))
                {
                    System.out.println(intCount + ") " + ints.toString());
                }
                else if(identifiers.get(i).equals("double"))
                {
                    System.out.println(doubleCount + ") " + doubles.toString());
                }
                else if(identifiers.get(i).equals("for"))
                {
                    System.out.println(forCount + ") " + forLoops.toString());
                }
                else if(identifiers.get(i).equals("if"))
                {
                    System.out.println(ifCount + ") " + ifStatements.toString());
                }
                else
                {
                    System.out.println(whileCount + ") " + whileStatements.toString());
                }
            }

        }
        catch(FileNotFoundException e)
        {
            System.out.println("File you are looking for can't be found or doesn't exist");
        }
    }
}
